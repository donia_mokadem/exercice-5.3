import { Component } from '@angular/core';
import { BadgesWalletRestServiceImplService } from 'src/app/rest';
import { DigitalBadge } from 'src/app/rest/model/models';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'badges-angular-frontend';

  constructor(private service: BadgesWalletRestServiceImplService) { }

  ngOnInit() {
    this.service.getMetadata().subscribe(metas => {
      console.log(metas);
      this.badges = metas;
      this.myDataArray = [...metas];
    })
  }

  badges?: Set<DigitalBadge>;
  myDataArray: DigitalBadge[] = [];
  displayedColumns: string[] = ['badgeId', 'serial', 'begin', 'end', 'imageSize'];

}
